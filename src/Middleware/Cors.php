<?php

namespace App\Middleware;

class Cors
{
    protected $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function __invoke($req, $res, $next)
    {
        $response = $next($req, $res);

        return $response
            ->withHeader('Access-Control-Allow-Origin', $this->container->get('settings')['frontend']['cors_origin'])
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS')
            ->withHeader('Access-Control-Allow-Credentials', 'true');
    }
}
