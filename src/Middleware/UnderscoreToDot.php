<?php

namespace App\Middleware;

class UnderscoreToDot
{
    public function __invoke($req, $res, $next)
    {
        $queryParams = $req->getQueryParams();
        foreach ($queryParams as $param => $value) {
            if (strpos($param, '_') === false) {
                continue;
            }

            $queryParams[str_replace('_', '.', $param)] = $value;
            unset($queryParams[$param]);
        }

        return $next($req->withQueryParams($queryParams), $res); 
    }
}
