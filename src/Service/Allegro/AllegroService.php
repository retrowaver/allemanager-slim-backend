<?php
namespace App\Service\Allegro;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Allegro\REST\Api;
use App\Service\TokenService;

class AllegroService
{
    protected $api;
    protected $apiResponseFactory;
    protected $tokenService;

    public function __construct(Api $phpAllegroRestApi, ApiResponseFactory $apiResponseFactory, TokenService $tokenService)
    {
        $this->api = $phpAllegroRestApi;
        $this->apiResponseFactory = $apiResponseFactory;
        $this->tokenService = $tokenService;
    }

    // Offers
    public function getOffers(RequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {
        $apiResponseBody = $this->api->sale->offers->get($request->getQueryParams());
        return $this->processResponse($response, $apiResponseBody);
    }

    public function getOffer(RequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {
        $apiResponseBody = $this->api->sale->offers($args['id'])->get();
        return $this->processResponse($response, $apiResponseBody);
    }

    public function postOffer(RequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {
        $apiResponseBody = $this->api->sale->offers->post($request->getParsedBody());
        return $this->processResponse($response, $apiResponseBody);
    }

    public function putOffer(RequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {
        $apiResponseBody = $this->api->sale->offers($args['id'])->put($request->getParsedBody());
        return $this->processResponse($response, $apiResponseBody);
    }

    public function deleteOffer(RequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {
        $apiResponseBody = $this->api->sale->offers($args['id'])->delete();
        return $this->processResponse($response, $apiResponseBody, ['successOnEmpty' => true]);
    }

    // Categories
    public function getCategories(RequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {
        $apiResponseBody = $this->api->sale->categories->get($request->getQueryParams());
        return $this->processResponse($response, $apiResponseBody);
    }

    public function getCategory(RequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {
        $apiResponseBody = $this->api->sale->categories($args['id'])->get();
        return $this->processResponse($response, $apiResponseBody);
    }

    // Categories' parameters
    public function getCategoryParameters(RequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {
        $apiResponseBody = $this->api->sale->categories($args['id'])->parameters->get();
        return $this->processResponse($response, $apiResponseBody);
    }

    // Offer publication
    public function publishOffers(RequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {
        $apiResponseBody = $this->api->sale->commands()->offer_publication($request->getParsedBody());
        return $this->processResponse($response, $apiResponseBody);
    }

    // Images
    public function uploadImage(RequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {
        $files = $request->getUploadedFiles();

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://upload.allegro.pl.allegrosandbox.pl/sale/images");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, reset($files)->getStream()->getContents());
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'accept: application/vnd.allegro.public.v1+json',
            'content-type: image/jpeg',
            'authorization: Bearer ' . $this->tokenService->getToken()
        ]);

        $apiResponseBody = curl_exec($ch);
        if ($apiResponseBody === false) {
            $apiResponseBody = '';
        }

        return $this->processResponse($response, $apiResponseBody);
    }

    /**
     *
     */
    protected function processResponse($response, string $apiResponseBody, array $options = []): ResponseInterface
    {
        $apiResponse = $this->apiResponseFactory->createApiResponse($apiResponseBody, $options);

        return $response
            ->withStatus($apiResponse->getHttpCode())
            ->write(
                $apiResponse->isError() ? $apiResponse->getErrorBody() : $apiResponse->getOriginalBody()
            )
        ;
    }
}
