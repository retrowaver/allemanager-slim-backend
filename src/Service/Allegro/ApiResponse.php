<?php
namespace App\Service\Allegro;

class ApiResponse
{
    protected $httpCode;
    protected $isError;
    protected $originalBody;
    protected $errorMessage;

    public function __construct(int $httpCode, bool $isError, string $originalBody, ?string $errorMessage = null)
    {
        $this->httpCode = $httpCode;
        $this->isError = $isError;
        $this->originalBody = $originalBody;
        $this->errorMessage = $errorMessage;
    }

    public function getHttpCode(): int
    {
        return $this->httpCode;
    }

    public function isError(): bool
    {
        return $this->isError;
    }

    public function getErrorBody(): ?string
    {
        return ($this->errorMessage !== null) ? json_encode(['error' => ['message' => $this->errorMessage]]) : null;
    }

    public function getOriginalBody(): string
    {
        return $this->originalBody;
    }
}
