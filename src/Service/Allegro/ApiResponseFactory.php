<?php
namespace App\Service\Allegro;

/**
 
 */
class ApiResponseFactory
{
    const ERR_DEFAULT = 'Nieznany błąd.';
    const ERR_UNAUTHENTICATED = 'Nie jesteś zalogowany.';

    /**
     * options reference here (successOnEmpty)
     */
    public function createApiResponse(string $apiResponseBody, array $options = []): ApiResponse
    {
        if ($apiResponseBody === '') {
            if (isset($options['successOnEmpty']) && $options['successOnEmpty'] === true) {
                return new ApiResponse(204, false, $apiResponseBody);
            }
            return new ApiResponse(400, true, $apiResponseBody, self::ERR_DEFAULT);
        }

        $decoded = json_decode($apiResponseBody, true);
        if ($decoded === null) {
            return new ApiResponse(400, true, $apiResponseBody, self::ERR_DEFAULT);
        }

        if (isset($decoded['error'])) {
            if ($decoded['error'] === 'invalid_token') {
                return new ApiResponse(401, true, $apiResponseBody, self::ERR_UNAUTHENTICATED);
            }
            // No more known cases of returning `error`
            return new ApiResponse(400, true, $apiResponseBody, self::ERR_DEFAULT);
        }

        if (isset($decoded['errors'])) {
            if (isset($decoded['errors'][0]['userMessage'])) {
                return new ApiResponse(400, true, $apiResponseBody, $decoded['errors'][0]['userMessage']);
            }

            return new ApiResponse(400, true, $apiResponseBody, self::ERR_DEFAULT);
        }

        return new ApiResponse(200, false, $apiResponseBody);
    }
}