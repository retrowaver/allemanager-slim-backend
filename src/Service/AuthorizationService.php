<?php

namespace App\Service;

class AuthorizationService
{
    protected $settings;

    public function __construct(array $settings)
    {
        $this->settings = $settings;
    }

    public function getAuthorizationUri(): string
    {
        return sprintf(
            '%s?%s',
            $this->settings['authorization_uri'],
            \http_build_query([
                'response_type' => 'code',
                'client_id' => $this->settings['client_id'],
                'redirect_uri' => $this->settings['redirect_uri']
            ])
        );
    }

    public function getAccessToken(string $code): string
    {
        $response = $this->makeGetTokenRequest($code);

        if ($response === false) {
            throw new \RuntimeException('Could not authorize');
        }

        $decoded = json_decode($response, true);

        if ($decoded === null || !isset($decoded['access_token'])) {
            throw new \RuntimeException('Could not authorize');
        }

        return $decoded['access_token'];
    }

    protected function makeGetTokenRequest(string $code)
    {
        $parameters = [
            'grant_type' => 'authorization_code',
            'code' => $code,
            'redirect_uri' => $this->settings['redirect_uri']
        ];
        $authstring = base64_encode($this->settings['client_id'] . ':' . $this->settings['client_secret']);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->settings['token_uri'] . '?' . http_build_query($parameters));
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Authorization: Basic ' . $authstring]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);

        return curl_exec($ch);
    }
}
