<?php

namespace App\Service;

class TokenService
{
    public function getToken(): ?string
    {
        return $_SESSION['access_token'] ?? null;
    }

    public function storeToken(?string $token): self
    {
        $_SESSION['access_token'] = $token;
        return $this;
    }

    public function clearToken(): self
    {
        $_SESSION['access_token'] = null;
        return $this;
    }
}
