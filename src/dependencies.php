<?php

use Slim\App;
use App\Service\TokenService;
use App\Service\AuthorizationService;
use App\Service\Allegro\AllegroService;
use App\Service\Allegro\ApiResponseFactory;

use App\Middleware\Cors;
use App\Middleware\UnderscoreToDot;

use Allegro\REST\Api;
use Allegro\REST\Sandbox;

return function (App $app) {
    $container = $app->getContainer();

    // view renderer
    $container['renderer'] = function ($c) {
        $settings = $c->get('settings')['renderer'];
        return new \Slim\Views\PhpRenderer($settings['template_path']);
    };

    // monolog
    $container['logger'] = function ($c) {
        $settings = $c->get('settings')['logger'];
        $logger = new \Monolog\Logger($settings['name']);
        $logger->pushProcessor(new \Monolog\Processor\UidProcessor());
        $logger->pushHandler(new \Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
        return $logger;
    };

    //

    $container['cors'] = function($c) {
        $cors = new Cors($c);
        return $cors;
    };

    $container['underscoreToDot'] = function($c) {
        $underscoreToDot = new UnderscoreToDot($c);
        return $underscoreToDot;
    };

    $container['tokenService'] = function($c) {
        $tokenService = new TokenService;
        return $tokenService;
    };

    $container['allegroApi'] = function($c) {
        if ($c->get('settings')['allegro']['sandbox']) {
            $api = new Sandbox(
                $c->get('settings')['allegro']['dev']['client_id'],
                $c->get('settings')['allegro']['dev']['client_secret'],
                $c->get('settings')['allegro']['dev']['client_id'],
                $c->get('settings')['allegro']['dev']['redirect_uri'],
                $c->tokenService->getToken()
            );
        } else {
            $api = new Api(
                $c->get('settings')['allegro']['prod']['client_id'],
                $c->get('settings')['allegro']['prod']['client_secret'],
                $c->get('settings')['allegro']['prod']['client_id'],
                $c->get('settings')['allegro']['prod']['redirect_uri'],
                $c->tokenService->getToken()
            );
        }
        return $api;
    };

    $container['apiResponseFactory'] = function($c) {
        $factory = new ApiResponseFactory;
        return $factory;
    };

    $container['allegro'] = function($c) {
        $allegro = new AllegroService($c['allegroApi'], $c['apiResponseFactory'], $c['tokenService']);
        return $allegro;
    };

    $container['authorizationService'] = function($c) {
        $settings = $c->get('settings')['allegro']['sandbox'] ? $c->get('settings')['allegro']['dev'] : $c->get('settings')['allegro']['prod'];
        $authorizationService = new AuthorizationService($settings);
        return $authorizationService;
    };
};
