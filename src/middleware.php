<?php

use Slim\App;

return function (App $app) {
    $app->add('cors');
    $app->add('underscoreToDot');
};
