<?php

use Slim\App;
use Slim\Http\Request;
use Slim\Http\Response;

return function (App $app) {
    $container = $app->getContainer();

    $app->get('/token', function ($request, $response, $args) {
        return $response->withJson([
            'access_token' => $this->tokenService->getToken()
        ]);
    });

    $app->get('/connect', function ($request, $response, $args) {
        $code = $request->getQueryParam('code');
        if (!$code) {
            return $response->withRedirect(
                $this->authorizationService->getAuthorizationUri()
            );
        }

        $this->tokenService->storeToken(
            $this->authorizationService->getAccessToken($code)
        );

        return $response->withRedirect(
            $this->settings['frontend']['redirect_uri']
        );
    });

    $app->get('/disconnect', function ($request, $response, $args) {
        $this->tokenService->clearToken();
        return $response;
    });

    // Offers
    $app->get('/sale/offers', function ($request, $response, $args) {
        return $this->allegro->getOffers($request, $response, $args);
    });

    $app->get('/sale/offers/{id}', function ($request, $response, $args) {
        return $this->allegro->getOffer($request, $response, $args);
    });

    $app->post('/sale/offers', function ($request, $response, $args) {
        return $this->allegro->postOffer($request, $response, $args);
    });

    $app->put('/sale/offers/{id}', function ($request, $response, $args) {
        return $this->allegro->putOffer($request, $response, $args);
    });

    $app->delete('/sale/offers/{id}', function ($request, $response, $args) {
        return $this->allegro->deleteOffer($request, $response, $args);
    });

    // Categories
    $app->get('/sale/categories', function ($request, $response, $args) {
        return $this->allegro->getCategories($request, $response, $args);
    });

    $app->get('/sale/categories/{id}', function ($request, $response, $args) {
        return $this->allegro->getCategory($request, $response, $args);
    });

    // Categories' parameters
    $app->get('/sale/categories/{id}/parameters', function ($request, $response, $args) {
        return $this->allegro->getCategoryParameters($request, $response, $args);
    });

    // Offer publication
    $app->put('/sale/offer-publication-commands', function ($request, $response, $args) {
        return $this->allegro->publishOffers($request, $response, $args);
    });

    // Images
    $app->post('/sale/images', function ($request, $response, $args) {
        return $this->allegro->uploadImage($request, $response, $args);
    });

    // Initial draft
    $app->get('/initialdraft', function ($request, $response, $args) {
        return $response
            ->withHeader('Content-type', 'application/json')
            ->write(json_encode(
                $this->settings['allegro'][$this->settings['allegro']['sandbox'] ? 'dev' : 'prod']['initial_draft_offer']
            ));
    });

    // CORS
    $app->options('/{routes:.+}', function ($request, $response, $args) {
        return $response;
    });

    // Catch-all route to serve a 404 Not Found page if none of the routes match
    // NOTE: make sure this route is defined last
    $app->map(['GET', 'POST', 'PUT', 'DELETE', 'PATCH'], '/{routes:.+}', function($req, $res) {
        $handler = $this->notFoundHandler; // handle using the default Slim page not found handler
        return $handler($req, $res);
    });
};
